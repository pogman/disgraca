size = 100000

xs = size * [0]
quantities = size * [1]

flag = True
i = 0

while flag and i < size:
    x = int(input("Entre um numero: "))

    if x > 0:
        repeated = False

        for j in range(i):
            if x == xs[j]:
                quantities[j] += 1
                repeated = True
        
        if not repeated:
            xs[i] = x
            i += 1
    else:
        flag = False

for i in range(size):
    print("O valor %d foi digitado %d vez(es)" % (xs[i], quantities[i]))
