size = 3

m = [[0 for b in range(size)] for a in range(size)]

for i in range(size):
    for j in range(size):
        m[i][j] = int(input("Entre o elemento da %d° linha e %d° coluna: " % (i + 1, j + 1)))

for i in range(size):
    soma_linhas = 0

    for j in range(size):
        if i != j:
            soma_linhas += m[i][j]

    soma_colunas = 0

    for j in range(size):
        if i != j:
            soma_colunas += m[j][i]

    m[i][i] = soma_linhas - soma_colunas

print(m)
