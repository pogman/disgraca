n = int(input("Entre a quantidade numeros da sequencia: "))
seq = n * [0]

for i in range(0, n):
    seq[i] = int(input("Entre o %d° elemento da sequencia: " % (i + 1)))

i = n - 1
q = seq[i] / seq[i - 1]

pg = True

while pg and i > 0:
    a2 = seq[i]
    a1 = seq[i - 1]

    if a2 / a1 != q:
        pg = False

    i -= 1

if pg:
    print("A sequencia é uma pg e a razão é: %d" % q)
else:
    print("A sequencia não é uma pg.")
