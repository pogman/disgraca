v = 10 * [0]

for i in range(0, 10):
    v[i] = int(input("Entre um numero: "))

sum = 0
product = 1

for i in v:
    sum += i
    product *= i

print("O somatorio é: %d" % (sum))
print("O produtorio é: %d" % (product))
