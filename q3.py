n = int(input("Entre a quantidade de numeros jogados: "))
numeros_jogados = n * [0]
numeros_sorteados = 6 * [0]

for i in range(0, n):
    numeros_jogados[i] = int(input("Entre o %d° numero jogado: " % (i + 1)))


print("Entre os numeros sorteados: ")
for i in range(0, 6):
    numeros_sorteados[i] = int(input("Entre o %d° numero sorteado: " % (i + 1)))

acertos = 0

for i in range(0, n):
    if numeros_jogados[i] in numeros_sorteados:
        acertos += 1

if acertos == 6:
    print("Fez a mega-sena")
elif acertos == 5:
    print("Fez a quina")
elif acertos == 4:
    print("Fez a quadra")
